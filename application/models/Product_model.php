<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	public $id;
	public $name;
	public $price;
	public $qty;
	public $desc;

	public function get_last_ten()
	{
			$query = $this->db->get('products', 10);
			return $query->result();
	}

	public function insert()
	{
			$this->id    	= $_POST['id']; 
			$this->name  	= $_POST['name'];
			$this->price 	= $_POST['price'];
			$this->qty		= $_post['qty'];
			$this->desc		= $_post['desc'];

			$this->db->insert('products', $this);
	}

	public function update()
	{
		$this->id    	= $_POST['id']; 
		$this->name  	= $_POST['name'];
		$this->price 	= $_POST['price'];
		$this->qty		= $_post['qty'];
		$this->desc		= $_post['desc'];

			$this->db->update('products', $this, array('id' => $_POST['id']));
	}

	public function delete()
	{
			$this->db->delete('products', array('id' =>  $_POST['id'])); 
	}

}

